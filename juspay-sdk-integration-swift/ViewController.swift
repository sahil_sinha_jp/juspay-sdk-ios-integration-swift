//
//  ViewController.swift
//  juspay-sdk-integration-swift
//
//  Created by Sahil Sinha on 24/03/22.
//

import UIKit
import HyperSDK

class ViewController: UIViewController {

    let hyperInstance = HyperServices();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
 
    
    func createInitiatePayload() -> [String: Any] {
        let innerPayload : [String: Any] = [
            "action": "initiate",
            "merchantId": "picasso",
            "clientId": "picasso_ios",
            "environment": "sandbox"
        ];
        
        let sdkPayload : [String: Any] = [
            "requestId": UUID().uuidString,
            "service": "in.juspay.hyperpay",
            "payload": innerPayload
        ]
        
        return sdkPayload
    }
    
    func createHyperCallbackHandler(callbackResult : (Optional<Dictionary<String, Any>>)) {
        
    }
    
    @IBAction func initiatePayments(_ sender: Any) {
        hyperInstance.initiate(
            self,
            payload: createInitiatePayload(),
            callback: createHyperCallbackHandler
        )
    }

    @IBAction func startPayments(_ sender: Any) {
        
    }
    
}

